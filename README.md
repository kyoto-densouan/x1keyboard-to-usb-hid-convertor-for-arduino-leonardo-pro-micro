SHARP Personal computer X1 Keyboard to USB HID Convertor for Arduino Leonardo/pro micro.  

Written by catsin.  
2018/01  

##Use Library##
import HID library.  

##Wiring##
X1 Keyboard - Arduino Leonardo/pro micro  
(Audio mini 3pin jack)  
 Vcc    -  5V  
 Signal -  pin2  
 GND    -  GND  

##Hardware##
Arduino Leonardo 開発ボード ソケット・ヘッダ付 A000057 Arduino https://www.amazon.co.jp/dp/B008A36R2Y/ref=cm_sw_r_tw_dp_U_x_NgUyAb43RDRH4  
ダイレクトUSB 超小型 ATmega32U4ボード Arduino Micro互換 (SS Micro) WONDER ELECTRONICS https://www.amazon.co.jp/dp/B0745C64RP/ref=cm_sw_r_tw_dp_U_x_nfUyAbT63CPCP  
マル信無線電機 3.5mmステレオジャック MJ-073H マル信無線電機 https://www.amazon.co.jp/dp/B011L1MLUI/ref=cm_sw_r_tw_dp_U_x_NkUyAb7X697ZS  

##other##
[CTRL]+[SHIFT]+HOME = Num Lock  
[CTRL]+[SHIFT]+[カナ] = frip kana mode
  
[CTRL]+[SHIFT]+↑ = Mouse move up  
[CTRL]+[SHIFT]+↓ = Mouse move down  
[CTRL]+[SHIFT]+← = Mouse move left  
[CTRL]+[SHIFT]+→ = Mouse move right  
[CTRL]+[SHIFT]+(tenkey)0 = Mouse left click  
[CTRL]+[SHIFT]+(tenkey), = Mouse right click  
  
X1turboのキーボードでeX1turboを操作する。: https://youtu.be/meqaD74u2wE  
x1turboのキーボードでミニチュアX1を操作する。: https://youtu.be/itatZacd7Z8  
X1turboのキーボードでxmilleniumを操作する。: https://youtu.be/ZjHbUoNGiTk  
X1turboのキーボードをUSB-HIDでWindowsに繋ぐ: https://youtu.be/XO_6NEIfrDc  

![](https://bitbucket.org/kyoto-densouan/x1keyboard-to-usb-hid-convertor-for-arduino-leonardo-pro-micro/raw/e3597dc7e02db1113258f115f9034f95f0346f03/IMG_20180123_015243.jpg)
![](https://bitbucket.org/kyoto-densouan/x1keyboard-to-usb-hid-convertor-for-arduino-leonardo-pro-micro/raw/e3597dc7e02db1113258f115f9034f95f0346f03/IMG_20180128_033054.jpg)
![](https://bitbucket.org/kyoto-densouan/x1keyboard-to-usb-hid-convertor-for-arduino-leonardo-pro-micro/raw/e3597dc7e02db1113258f115f9034f95f0346f03/IMG_20180128_135307.jpg)
![](https://bitbucket.org/kyoto-densouan/x1keyboard-to-usb-hid-convertor-for-arduino-leonardo-pro-micro/raw/e3597dc7e02db1113258f115f9034f95f0346f03/IMG_20180128_135314.jpg)

