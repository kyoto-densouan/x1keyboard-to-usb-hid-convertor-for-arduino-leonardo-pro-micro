#include "ModKeyboard.h"
#include "Mouse.h"
#include "IntervalCheck.h"
#include "X1Keyboard.h"

#define D_PIN_X1_KEYBOARD   (2)
#define D_PIN_LED           (13)

#define D_INT_X1_KEYBOARD   (1)

X1Keyboard  inkey_x1_( D_PIN_X1_KEYBOARD, D_INT_X1_KEYBOARD );    // X1キー入力

IntervalCheck inkey_led_( 200, false );                           // モニタリングLED点灯期間

#define D_KEY_MASK_FULL     (0x0000ffff)
#define D_KEY_MASK_KEYCODE  (0x000000ff)
#define D_KEY_MASK_TENKEY   (0x00008000)
#define D_KEY_MASK_INKEY    (0x00004000)
#define D_KEY_MASK_REPEAT   (0x00002000)
#define D_KEY_MASK_GRAPH    (0x00001000)
#define D_KEY_MASK_CAPSLOCK (0x00000800)
#define D_KEY_MASK_KANA     (0x00000400)
#define D_KEY_MASK_SHIFT    (0x00000200)
#define D_KEY_MASK_CTRL     (0x00000100)

bool  s_kana_status = false;                                      // カナキーのトグル

#define D_MOUSE_MOVE_DISTANCE (8)

void setup() {
  //  キーコード送信間隔の設定[ms]
  Keyboard.setInterval( 30 );

  Keyboard.begin();
  Mouse.begin();
  
  pinMode( D_PIN_LED, OUTPUT );

  Serial.begin( 115200 );
}

void loop() {
  int l = 0;
  unsigned long d = 0;

  // 割り込み禁止区間
  {
    noInterrupts();
    // キー入力バッファに文字があれば取得
    l = inkey_x1_.size();
    if( l > 0 ) {
      d = inkey_x1_.get_code();
    }
    interrupts();
  }

  // モニタリングLED消灯  
  if( inkey_led_.check() == true ) {
    digitalWrite( D_PIN_LED, LOW );
  }

  if( l > 0 ) {
    // モニタリングLED点灯
    digitalWrite( D_PIN_LED, HIGH );
    inkey_led_.reset();

    Serial.println( d, HEX );

    unsigned int code;
    code = (unsigned int)(d & D_KEY_MASK_FULL);

    bool enable_inkey = true;

    Keyboard.clearReport();

    // カナキーはトグル
    if( (code & D_KEY_MASK_KANA) == 0x0000 ) {
      if( s_kana_status == false ) {
        if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL)) == 0x0000 ) {
          // SHIFT+CTRL押しながらの場合は状態だけを裏返す
        } else {
          Keyboard.pressNative( 0x88 );
        }
        s_kana_status = true;
      }
    } else {
      if( s_kana_status == true ) {
        Keyboard.pressNative( 0x88 );
        s_kana_status = false;
      }
    }

    // NumLock操作
    if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x000C ) {
      Keyboard.pressNative( 0x53 );
      enable_inkey = false; // キー入力を無効にする+
    }

    // Mouse操作
    {
      int mouse_x = 0;
      int mouse_y = 0;
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x001d ) {
        mouse_x = -D_MOUSE_MOVE_DISTANCE;
      }
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x001c ) {
        mouse_x = D_MOUSE_MOVE_DISTANCE;
      }
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x001e ) {
        mouse_y = -D_MOUSE_MOVE_DISTANCE;
      }
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x001f ) {
        mouse_y = D_MOUSE_MOVE_DISTANCE;
      }
      if( mouse_x !=  0 || mouse_y != 0 ){
        Mouse.move( mouse_x, mouse_y ); 
        enable_inkey = false; // キー入力を無効にする+
      }
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x0030 ) {
        Mouse.click(MOUSE_LEFT);
        enable_inkey = false; // キー入力を無効にする+
      }
      if( (code & (D_KEY_MASK_SHIFT | D_KEY_MASK_CTRL | D_KEY_MASK_TENKEY | D_KEY_MASK_INKEY | D_KEY_MASK_KEYCODE)) == 0x002c ) {
        Mouse.click(MOUSE_RIGHT);
        enable_inkey = false; // キー入力を無効にする+
      }
    }

    // キーコード変換・送信
    if( ((code & D_KEY_MASK_INKEY) == 0x0000) && (enable_inkey == true) )  {
      // キー入力あり
      //--------------------------------------------------------------------------------
      if( (code & D_KEY_MASK_GRAPH) == 0x0000 ) {
        // GRAPHキー押下状態
        if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
          // テンキー側
          switch( code & D_KEY_MASK_KEYCODE ) {
          case  0x000b:   Keyboard.pressNative( 0xe2, 0x4a );   break;  // HOME
          case  0x001d:   Keyboard.pressNative( 0xe2, 0x50 );   break;  // KEYPAD ←
          case  0x001c:   Keyboard.pressNative( 0xe2, 0x4f );   break;  // KEYPAD →
          case  0x001e:   Keyboard.pressNative( 0xe2, 0x52 );   break;  // KEYPAD ↑
          case  0x001f:   Keyboard.pressNative( 0xe2, 0x51 );   break;  // KEYPAD ↓
          case  0x009b:   Keyboard.pressNative( 0xe2, 0x55 );   break;  // KEYPAD *
          case  0x009d:   Keyboard.pressNative( 0xe2, 0x57 );   break;  // KEYPAD +
          case  0x009f:   Keyboard.pressNative( 0xe2, 0x36 );   break;  // FULL , <- KEYPAD ,
          case  0x0091:   Keyboard.pressNative( 0xe2, 0x63 );   break;  // KEYPAD .
          case  0x009c:   Keyboard.pressNative( 0xe2, 0x56 );   break;  // KEYPAD -
          case  0x009e:   Keyboard.pressNative( 0xe2, 0x54 );   break;  // KEYPAD /
          case  0x0090:   Keyboard.pressNative( 0xe2, 0xe1, 0x2d );  break;  // FULL SHIFT - <- KEYPAD
          case  0x000d:   Keyboard.pressNative( 0xe2, 0x58 );   break;  // KEYPAD ENTER
          case  0x0099:   Keyboard.pressNative( 0xe2, 0x59 );   break;  // KEYPAD 1
          case  0x0092:   Keyboard.pressNative( 0xe2, 0x5a );   break;  // KEYPAD 2
          case  0x0098:   Keyboard.pressNative( 0xe2, 0x5b );   break;  // KEYPAD 3
          case  0x0095:   Keyboard.pressNative( 0xe2, 0x5c );   break;  // KEYPAD 4
          case  0x0096:   Keyboard.pressNative( 0xe2, 0x5d );   break;  // KEYPAD 5
          case  0x0094:   Keyboard.pressNative( 0xe2, 0x5e );   break;  // KEYPAD 6
          case  0x009a:   Keyboard.pressNative( 0xe2, 0x5f );   break;  // KEYPAD 7
          case  0x0093:   Keyboard.pressNative( 0xe2, 0x60 );   break;  // KEYPAD 8
          case  0x0097:   Keyboard.pressNative( 0xe2, 0x61 );   break;  // KEYPAD 9
          case  0x008f:   Keyboard.pressNative( 0xe2, 0x62 );   break;  // KEYPAD 0
          case  0x0071:   Keyboard.pressNative( 0xe2, 0x3a );   break;  // F1
          case  0x0072:   Keyboard.pressNative( 0xe2, 0x3b );   break;  // F2
          case  0x0073:   Keyboard.pressNative( 0xe2, 0x3c );   break;  // F3
          case  0x0074:   Keyboard.pressNative( 0xe2, 0x3d );   break;  // F4
          case  0x0075:   Keyboard.pressNative( 0xe2, 0x3e );   break;  // F5
          case  0x00e1:   Keyboard.pressNative( 0xe2, 0x42 );   break;  // F9 <- [COPY]
          case  0x00e2:   Keyboard.pressNative( 0xe2, 0x41 );   break;  // F8 <- [HELP]
          case  0x00e8:   Keyboard.pressNative( 0xe2, 0x62, 0x32 );   break;  // FULL ] <- XFER
          case  0x00eb:   Keyboard.pressNative( 0xe2, 0x40 );   break;  // F7 <- [RollDown]
          case  0x00ec:   Keyboard.pressNative( 0xe2, 0x3f );   break;  // F6 <- [RollUp]
          default:  Keyboard.pressNative( 0xe2 );   break;
          }
        } else {
          // フルキー側
          switch( code & D_KEY_MASK_KEYCODE ) {
          case   0x0020:  Keyboard.pressNative( 0xe2, 0x2c );   break;  // SPACE
          case   0x0008:  Keyboard.pressNative( 0xe2, 0x4c );   break;  // DEL
          case   0x0009:  Keyboard.pressNative( 0xe2, 0x2b );   break;  // TAB
          case   0x000d:  Keyboard.pressNative( 0xe2, 0x28 );   break;  // Enter
          case   0x0013:  Keyboard.pressNative( 0xe2, 0x48 );   break;  // Pause <- BREAK
          case   0x001b:  Keyboard.pressNative( 0xe2, 0x29 );   break;  // ESC
          case   0x008c:  Keyboard.pressNative( 0xe2, 0x2d );   break;  // -
          case   0x008b:  Keyboard.pressNative( 0xe2, 0x2e );   break;  // ^
          case   0x00fb:  Keyboard.pressNative( 0xe2, 0x89 );   break;  // \
          case   0x008a:  Keyboard.pressNative( 0xe2, 0x2f );   break;  // @
          case   0x00fc:  Keyboard.pressNative( 0xe2, 0x30 );   break;  // [
          case   0x0089:  Keyboard.pressNative( 0xe2, 0x33 );   break;  // ;
          case   0x00fd:  Keyboard.pressNative( 0xe2, 0x34 );   break;  // :
          case   0x00e8:  Keyboard.pressNative( 0xe2, 0x32 );   break;  // ]
          case   0x0087:  Keyboard.pressNative( 0xe2, 0x36 );   break;  // ,
          case   0x0088:  Keyboard.pressNative( 0xe2, 0x37 );   break;  // .
          case   0x00fe:  Keyboard.pressNative( 0xe2, 0x38 );   break;  // /
          case   0x00ff:  Keyboard.pressNative( 0xe2, 0x87 );   break;  // (\)
          case   0x00f1:  Keyboard.pressNative( 0xe2, 0x1e );   break;  // 1
          case   0x00f2:  Keyboard.pressNative( 0xe2, 0x1f );   break;  // 2
          case   0x00f3:  Keyboard.pressNative( 0xe2, 0x20 );   break;  // 3
          case   0x00f4:  Keyboard.pressNative( 0xe2, 0x21 );   break;  // 4
          case   0x00f5:  Keyboard.pressNative( 0xe2, 0x22 );   break;  // 5
          case   0x00f6:  Keyboard.pressNative( 0xe2, 0x23 );   break;  // 6
          case   0x00f7:  Keyboard.pressNative( 0xe2, 0x24 );   break;  // 7
          case   0x00f8:  Keyboard.pressNative( 0xe2, 0x25 );   break;  // 8
          case   0x00f9:  Keyboard.pressNative( 0xe2, 0x26 );   break;  // 9
          case   0x00fa:  Keyboard.pressNative( 0xe2, 0x27 );   break;  // 0
          case   0x007f:  Keyboard.pressNative( 0xe2, 0x04 );   break;  // a
          case   0x0084:  Keyboard.pressNative( 0xe2, 0x05 );   break;  // b
          case   0x0082:  Keyboard.pressNative( 0xe2, 0x06 );   break;  // c
          case   0x00ea:  Keyboard.pressNative( 0xe2, 0x07 );   break;  // d
          case   0x00e2:  Keyboard.pressNative( 0xe2, 0x08 );   break;  // e
          case   0x00eb:  Keyboard.pressNative( 0xe2, 0x09 );   break;  // f
          case   0x00ec:  Keyboard.pressNative( 0xe2, 0x0a );   break;  // g
          case   0x00ed:  Keyboard.pressNative( 0xe2, 0x0b );   break;  // h
          case   0x00e7:  Keyboard.pressNative( 0xe2, 0x0c );   break;  // i
          case   0x00ee:  Keyboard.pressNative( 0xe2, 0x0d );   break;  // j
          case   0x00ef:  Keyboard.pressNative( 0xe2, 0x0e );   break;  // k
          case   0x008e:  Keyboard.pressNative( 0xe2, 0x0f );   break;  // l
          case   0x0086:  Keyboard.pressNative( 0xe2, 0x10 );   break;  // m
          case   0x0085:  Keyboard.pressNative( 0xe2, 0x11 );   break;  // n
          case   0x00f0:  Keyboard.pressNative( 0xe2, 0x12 );   break;  // o
          case   0x008d:  Keyboard.pressNative( 0xe2, 0x13 );   break;  // p
          case   0x00e0:  Keyboard.pressNative( 0xe2, 0x14 );   break;  // q
          case   0x00e3:  Keyboard.pressNative( 0xe2, 0x15 );   break;  // r
          case   0x00e9:  Keyboard.pressNative( 0xe2, 0x16 );   break;  // s
          case   0x00e4:  Keyboard.pressNative( 0xe2, 0x17 );   break;  // t
          case   0x00e6:  Keyboard.pressNative( 0xe2, 0x18 );   break;  // u
          case   0x0083:  Keyboard.pressNative( 0xe2, 0x19 );   break;  // v
          case   0x00e1:  Keyboard.pressNative( 0xe2, 0x1a );   break;  // w
          case   0x0081:  Keyboard.pressNative( 0xe2, 0x1b );   break;  // x
          case   0x00e5:  Keyboard.pressNative( 0xe2, 0x1c );   break;  // y
          case   0x0080:  Keyboard.pressNative( 0xe2, 0x1d );   break;  // z
          default:  Keyboard.pressNative( 0xe2 );   break;
          }
        }
      //--------------------------------------------------------------------------------
      } else if( (code & D_KEY_MASK_CTRL) == 0 ) {
        // CTRLキー押下状態
        if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
          // テンキー側
          switch( code & D_KEY_MASK_KEYCODE ) {
          case  0x000b:   Keyboard.pressNative( 0xe0, 0x4a );   break;  // HOME
          case  0x000c:   Keyboard.pressNative( 0xe0, 0xe1, 0x4a );   break;  // CLR(HOME)
          case  0x001d:   Keyboard.pressNative( 0xe0, 0x50 );   break;  // KEYPAD ←
          case  0x001c:   Keyboard.pressNative( 0xe0, 0x4f );   break;  // KEYPAD →
          case  0x001e:   Keyboard.pressNative( 0xe0, 0x52 );   break;  // KEYPAD ↑
          case  0x001f:   Keyboard.pressNative( 0xe0, 0x51 );   break;  // KEYPAD ↓
          case  0x002a:   Keyboard.pressNative( 0xe0, 0x55 );   break;  // KEYPAD *
          case  0x002b:   Keyboard.pressNative( 0xe0, 0x57 );   break;  // KEYPAD +
          case  0x002c:   Keyboard.pressNative( 0xe0, 0x36 );   break;  // FULL , <- KEYPAD ,
          case  0x002e:   Keyboard.pressNative( 0xe0, 0x63 );   break;  // KEYPAD .
          case  0x002d:   Keyboard.pressNative( 0xe0, 0x56 );   break;  // KEYPAD -
          case  0x002f:   Keyboard.pressNative( 0xe0, 0x54 );   break;  // KEYPAD /
          case  0x003d:   Keyboard.pressNative( 0xe0, 0xe2, 0x2d );   break;  // FULL SHIFT - <- KEYPAD
          case  0x000d:   Keyboard.pressNative( 0xe0, 0x58 );   break;  // KEYPAD ENTER
          case  0x0031:   Keyboard.pressNative( 0xe0, 0x59 );   break;  // KEYPAD 1
          case  0x0032:   Keyboard.pressNative( 0xe0, 0x5a );   break;  // KEYPAD 2
          case  0x0033:   Keyboard.pressNative( 0xe0, 0x5b );   break;  // KEYPAD 3
          case  0x0034:   Keyboard.pressNative( 0xe0, 0x5c );   break;  // KEYPAD 4
          case  0x0035:   Keyboard.pressNative( 0xe0, 0x5d );   break;  // KEYPAD 5
          case  0x0036:   Keyboard.pressNative( 0xe0, 0x5e );   break;  // KEYPAD 6
          case  0x0037:   Keyboard.pressNative( 0xe0, 0x5f );   break;  // KEYPAD 7
          case  0x0038:   Keyboard.pressNative( 0xe0, 0x60 );   break;  // KEYPAD 8
          case  0x0039:   Keyboard.pressNative( 0xe0, 0x61 );   break;  // KEYPAD 9
          case  0x0030:   Keyboard.pressNative( 0xe0, 0x62 );   break;  // KEYPAD 0
          case  0x0071:   Keyboard.pressNative( 0xe0, 0x3a );   break;  // F1
          case  0x0072:   Keyboard.pressNative( 0xe0, 0x3b );   break;  // F2
          case  0x0073:   Keyboard.pressNative( 0xe0, 0x3c );   break;  // F3
          case  0x0074:   Keyboard.pressNative( 0xe0, 0x3d );   break;  // F4
          case  0x0075:   Keyboard.pressNative( 0xe0, 0x3e );   break;  // F5
          case  0x00e1:   Keyboard.pressNative( 0xe0, 0x42 );   break;  // F9 <- [COPY]
          case  0x00e2:   Keyboard.pressNative( 0xe0, 0x41 );   break;  // F8 <- [HELP]
          case  0x00e8:   Keyboard.pressNative( 0xe0, 0x62, 0x32 );   break;  // FULL ] <- XFER
          case  0x00eb:   Keyboard.pressNative( 0xe0, 0x40 );   break;  // F7 <- [RollDown]
          case  0x00ec:   Keyboard.pressNative( 0xe0, 0x3f );   break;  // F6 <- [RollUp]
          default:  Keyboard.pressNative( 0xe0 );   break;
          }
        } else {
          // フルキー側
          switch( code & D_KEY_MASK_KEYCODE ) {
          case  0x0020:   Keyboard.pressNative( 0xe0, 0x2c );   break;  // SPACE
          case  0x001b:   Keyboard.pressNative( 0xe0, 0x29 );   break;  // ESC
          case  0x002d:   Keyboard.pressNative( 0xe0, 0x2d );   break;  // -
          case  0x005e:   Keyboard.pressNative( 0xe0, 0x2e );   break;  // ^
          case  0x005c:   Keyboard.pressNative( 0xe0, 0x89 );   break;  // \
          case  0x0040:   Keyboard.pressNative( 0xe0, 0x2f );   break;  // @
          case  0x005b:   Keyboard.pressNative( 0xe0, 0x30 );   break;  // [
          case  0x003b:   Keyboard.pressNative( 0xe0, 0x33 );   break;  // ;
          case  0x003a:   Keyboard.pressNative( 0xe0, 0x34 );   break;  // :
          case  0x005d:   Keyboard.pressNative( 0xe0, 0x32 );   break;  // ]
          case  0x002c:   Keyboard.pressNative( 0xe0, 0x36 );   break;  // ,
          case  0x002e:   Keyboard.pressNative( 0xe0, 0x37 );   break;  // .
          case  0x002f:   Keyboard.pressNative( 0xe0, 0x38 );   break;  // /
          case  0x005f:   Keyboard.pressNative( 0xe0, 0x87 );   break;  // (\)
          case  0x0031:   Keyboard.pressNative( 0xe0, 0x1e );   break;  // 1
          case  0x0032:   Keyboard.pressNative( 0xe0, 0x1f );   break;  // 2
          case  0x0033:   Keyboard.pressNative( 0xe0, 0x20 );   break;  // 3
          case  0x0034:   Keyboard.pressNative( 0xe0, 0x21 );   break;  // 4
          case  0x0035:   Keyboard.pressNative( 0xe0, 0x22 );   break;  // 5
          case  0x0036:   Keyboard.pressNative( 0xe0, 0x23 );   break;  // 6
          case  0x0037:   Keyboard.pressNative( 0xe0, 0x24 );   break;  // 7
          case  0x0038:   Keyboard.pressNative( 0xe0, 0x25 );   break;  // 8
          case  0x0039:   Keyboard.pressNative( 0xe0, 0x26 );   break;  // 9
          case  0x0030:   Keyboard.pressNative( 0xe0, 0x27 );   break;  // 0
          case  0x0001:   Keyboard.pressNative( 0xe0, 0x04 );   break;  // a
          case  0x0002:   Keyboard.pressNative( 0xe0, 0x05 );   break;  // b
          case  0x0003:   Keyboard.pressNative( 0xe0, 0x06 );   break;  // c
          case  0x0004:   Keyboard.pressNative( 0xe0, 0x07 );   break;  // d
          case  0x0005:   Keyboard.pressNative( 0xe0, 0x08 );   break;  // e
          case  0x0006:   Keyboard.pressNative( 0xe0, 0x09 );   break;  // f
          case  0x0007:   Keyboard.pressNative( 0xe0, 0x0a );   break;  // g
          case  0x0008:   Keyboard.pressNative( 0xe0, 0x0b );   break;  // h
          case  0x0009:   Keyboard.pressNative( 0xe0, 0x0c );   break;  // i
          case  0x000a:   Keyboard.pressNative( 0xe0, 0x0d );   break;  // j
          case  0x000b:   Keyboard.pressNative( 0xe0, 0x0e );   break;  // k
          case  0x000c:   Keyboard.pressNative( 0xe0, 0x0f );   break;  // l
          case  0x000d:   Keyboard.pressNative( 0xe0, 0x10 );   break;  // m
          case  0x000e:   Keyboard.pressNative( 0xe0, 0x11 );   break;  // n
          case  0x000f:   Keyboard.pressNative( 0xe0, 0x12 );   break;  // o
          case  0x0010:   Keyboard.pressNative( 0xe0, 0x13 );   break;  // p
          case  0x0011:   Keyboard.pressNative( 0xe0, 0x14 );   break;  // q
          case  0x0012:   Keyboard.pressNative( 0xe0, 0x15 );   break;  // r
          case  0x0013:   Keyboard.pressNative( 0xe0, 0x16 );   break;  // s
          case  0x0014:   Keyboard.pressNative( 0xe0, 0x17 );   break;  // t
          case  0x0015:   Keyboard.pressNative( 0xe0, 0x18 );   break;  // u
          case  0x0016:   Keyboard.pressNative( 0xe0, 0x19 );   break;  // v
          case  0x0017:   Keyboard.pressNative( 0xe0, 0x1a );   break;  // w
          case  0x0018:   Keyboard.pressNative( 0xe0, 0x1b );   break;  // x
          case  0x0019:   Keyboard.pressNative( 0xe0, 0x1c );   break;  // y
          case  0x001a:   Keyboard.pressNative( 0xe0, 0x1d );   break;  // z
          default:  Keyboard.pressNative( 0xe0 );   break;
          }
        }
      //--------------------------------------------------------------------------------
      } else if( (code & D_KEY_MASK_KANA) == 0x0000 ) {
        // かなキー押下状態
        if( (code & D_KEY_MASK_SHIFT) == 0 ) {
          // シフトキー押下状態
          if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
            // テンキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x000c:   Keyboard.pressNative( 0xe1, 0x4a );   break;  // CLR(HOME)
            case  0x001d:   Keyboard.pressNative( 0xe1, 0x50 );   break;  // KEYPAD ←
            case  0x001c:   Keyboard.pressNative( 0xe1, 0x4f );   break;  // KEYPAD →
            case  0x001e:   Keyboard.pressNative( 0xe1, 0x52 );   break;  // KEYPAD ↑
            case  0x001f:   Keyboard.pressNative( 0xe1, 0x51 );   break;  // KEYPAD ↓
            case  0x002a:   Keyboard.pressNative( 0xe1, 0x55 );   break;  // KEYPAD *
            case  0x002b:   Keyboard.pressNative( 0xe1, 0x57 );   break;  // KEYPAD +
            case  0x002c:   Keyboard.pressNative( 0xe1, 0x36 );   break;  // FULL , <- KEYPAD ,
            case  0x002e:   Keyboard.pressNative( 0xe1, 0x63 );   break;  // KEYPAD .
            case  0x002d:   Keyboard.pressNative( 0xe1, 0x56 );   break;  // KEYPAD -
            case  0x002f:   Keyboard.pressNative( 0xe1, 0x54 );   break;  // KEYPAD /
            case  0x003d:   Keyboard.pressNative( 0xe1, 0x2d );   break;  // FULL SHIFT - <- KEYPAD
            case  0x000d:   Keyboard.pressNative( 0xe1, 0x58 );   break;  // KEYPAD ENTER
            case  0x0031:   Keyboard.pressNative( 0xe1, 0x59 );   break;  // KEYPAD 1
            case  0x0032:   Keyboard.pressNative( 0xe1, 0x5a );   break;  // KEYPAD 2
            case  0x0033:   Keyboard.pressNative( 0xe1, 0x5b );   break;  // KEYPAD 3
            case  0x0034:   Keyboard.pressNative( 0xe1, 0x5c );   break;  // KEYPAD 4
            case  0x0035:   Keyboard.pressNative( 0xe1, 0x5d );   break;  // KEYPAD 5
            case  0x0036:   Keyboard.pressNative( 0xe1, 0x5e );   break;  // KEYPAD 6
            case  0x0037:   Keyboard.pressNative( 0xe1, 0x5f );   break;  // KEYPAD 7
            case  0x0038:   Keyboard.pressNative( 0xe1, 0x60 );   break;  // KEYPAD 8
            case  0x0039:   Keyboard.pressNative( 0xe1, 0x61 );   break;  // KEYPAD 9
            case  0x0030:   Keyboard.pressNative( 0xe1, 0x62 );   break;  // KEYPAD 0
            case  0x0076:   Keyboard.pressNative( 0xe1, 0x3a );   break;  // F6
            case  0x0077:   Keyboard.pressNative( 0xe1, 0x3b );   break;  // F7
            case  0x0078:   Keyboard.pressNative( 0xe1, 0x3c );   break;  // F8
            case  0x0079:   Keyboard.pressNative( 0xe1, 0x3d );   break;  // F9
            case  0x007a:   Keyboard.pressNative( 0xe1, 0x3e );   break;  // F10
            case  0x00e1:   Keyboard.pressNative( 0x42 );   break;  // F9 <- [COPY]
            case  0x00e2:   Keyboard.pressNative( 0x41 );   break;  // F8 <- [HELP]
            case  0x00e8:   Keyboard.pressNative( 0x62, 0x32 );   break;  // FULL ] <- XFER
            case  0x00eb:   Keyboard.pressNative( 0x40 );   break;  // F7 <- [RollDown]
            case  0x00ec:   Keyboard.pressNative( 0x3f );   break;  // F6 <- [RollUp]
            default:  Keyboard.pressNative( 0xe1 ); break;
            }
          } else {
            // フルキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x0020:   Keyboard.pressNative( 0xe1, 0x2c );   break;  // SPACE
            case  0x0012:   Keyboard.pressNative( 0xe1, 0x49 );   break;  // INS(DEL)
            case  0x0009:   Keyboard.pressNative( 0xe1, 0x2b );   break;  // TAB
            case  0x000d:   Keyboard.pressNative( 0xe1, 0x28 );   break;  // Enter
            case  0x0003:   Keyboard.pressNative( 0xe1, 0x48 );   break;  // Pause <- BREAK
            case  0x001b:   Keyboard.pressNative( 0xe1, 0x29 );   break;  // ESC
            case  0x00ce:   Keyboard.pressNative( 0xe1, 0x2d );   break;  // ホ
            case  0x00cd:   Keyboard.pressNative( 0xe1, 0x2e );   break;  // ヘ
            case  0x00b0:   Keyboard.pressNative( 0xe1, 0x89 );   break;  // ー
            case  0x00de:   Keyboard.pressNative( 0xe1, 0x2f );   break;  // @
            case  0x00a2:   Keyboard.pressNative( 0xe1, 0x30 );   break;  // 「
            case  0x00da:   Keyboard.pressNative( 0xe1, 0x33 );   break;  // レ
            case  0x00b9:   Keyboard.pressNative( 0xe1, 0x34 );   break;  // ヶ
            case  0x00a3:   Keyboard.pressNative( 0xe1, 0x32 );   break;  // 」
            case  0x00a4:   Keyboard.pressNative( 0xe1, 0x36 );   break;  // 、
            case  0x00a1:   Keyboard.pressNative( 0xe1, 0x37 );   break;  // 。
            case  0x00a5:   Keyboard.pressNative( 0xe1, 0x38 );   break;  // ・
            case  0x00a0:   Keyboard.pressNative( 0xe1, 0x87 );   break;  // ロ
            case  0x00c7:   Keyboard.pressNative( 0xe1, 0x1e );   break;  // ヌ
            case  0x00cc:   Keyboard.pressNative( 0xe1, 0x1f );   break;  // フ
            case  0x00a7:   Keyboard.pressNative( 0xe1, 0x20 );   break;  // ァ
            case  0x00a9:   Keyboard.pressNative( 0xe1, 0x21 );   break;  // ゥ
            case  0x00aa:   Keyboard.pressNative( 0xe1, 0x22 );   break;  // ェ
            case  0x00ab:   Keyboard.pressNative( 0xe1, 0x23 );   break;  // ォ
            case  0x00ac:   Keyboard.pressNative( 0xe1, 0x24 );   break;  // ャ
            case  0x00ad:   Keyboard.pressNative( 0xe1, 0x25 );   break;  // ュ
            case  0x00ae:   Keyboard.pressNative( 0xe1, 0x26 );   break;  // ョ
            case  0x00a6:   Keyboard.pressNative( 0xe1, 0x27 );   break;  // ヲ
            case  0x00c1:   Keyboard.pressNative( 0xe1, 0x04 );   break;  // チ
            case  0x00ba:   Keyboard.pressNative( 0xe1, 0x05 );   break;  // コ
            case  0x00bf:   Keyboard.pressNative( 0xe1, 0x06 );   break;  // ソ
            case  0x00bc:   Keyboard.pressNative( 0xe1, 0x07 );   break;  // シ
            case  0x00a8:   Keyboard.pressNative( 0xe1, 0x08 );   break;  // イ
            case  0x00ca:   Keyboard.pressNative( 0xe1, 0x09 );   break;  // ハ
            case  0x00b7:   Keyboard.pressNative( 0xe1, 0x0a );   break;  // キ
            case  0x00b8:   Keyboard.pressNative( 0xe1, 0x0b );   break;  // ク
            case  0x00c6:   Keyboard.pressNative( 0xe1, 0x0c );   break;  // ニ
            case  0x00cf:   Keyboard.pressNative( 0xe1, 0x0d );   break;  // マ
            case  0x00c9:   Keyboard.pressNative( 0xe1, 0x0e );   break;  // ノ
            case  0x00d8:   Keyboard.pressNative( 0xe1, 0x0f );   break;  // リ
            case  0x00d3:   Keyboard.pressNative( 0xe1, 0x10 );   break;  // モ
            case  0x00d0:   Keyboard.pressNative( 0xe1, 0x11 );   break;  // ミ
            case  0x00d7:   Keyboard.pressNative( 0xe1, 0x12 );   break;  // タ
            case  0x00be:   Keyboard.pressNative( 0xe1, 0x13 );   break;  // セ
            case  0x00c0:   Keyboard.pressNative( 0xe1, 0x14 );   break;  // タ
            case  0x00bd:   Keyboard.pressNative( 0xe1, 0x15 );   break;  // ス
            case  0x00c4:   Keyboard.pressNative( 0xe1, 0x16 );   break;  // ト
            case  0x00b6:   Keyboard.pressNative( 0xe1, 0x17 );   break;  // カ
            case  0x00c5:   Keyboard.pressNative( 0xe1, 0x18 );   break;  // ナ
            case  0x00cb:   Keyboard.pressNative( 0xe1, 0x19 );   break;  // ヒ
            case  0x00c3:   Keyboard.pressNative( 0xe1, 0x1a );   break;  // テ
            case  0x00bb:   Keyboard.pressNative( 0xe1, 0x1b );   break;  // サ
            case  0x00dd:   Keyboard.pressNative( 0xe1, 0x1c );   break;  // ン
            case  0x00af:   Keyboard.pressNative( 0xe1, 0x1d );   break;  // ッ
            default:  Keyboard.pressNative( 0xe1 ); break;
            }
          }
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        } else {
          // シフトキーなし
          if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
            // テンキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x000b:   Keyboard.pressNative( 0x4a );   break;  // HOME
            case  0x001d:   Keyboard.pressNative( 0x50 );   break;  // KEYPAD ←
            case  0x001c:   Keyboard.pressNative( 0x4f );   break;  // KEYPAD →
            case  0x001e:   Keyboard.pressNative( 0x52 );   break;  // KEYPAD ↑
            case  0x001f:   Keyboard.pressNative( 0x51 );   break;  // KEYPAD ↓
            case  0x002a:   Keyboard.pressNative( 0x55 );   break;  // KEYPAD *
            case  0x002b:   Keyboard.pressNative( 0x57 );   break;  // KEYPAD +
            case  0x002c:   Keyboard.pressNative( 0x36 );   break;  // FULL , <- KEYPAD ,
            case  0x002e:   Keyboard.pressNative( 0x63 );   break;  // KEYPAD .
            case  0x002d:   Keyboard.pressNative( 0x56 );   break;  // KEYPAD -
            case  0x002f:   Keyboard.pressNative( 0x54 );   break;  // KEYPAD /
            case  0x003d:   Keyboard.pressNative( 0xe1, 0x2d );  break;  // FULL SHIFT - <- KEYPAD
            case  0x000d:   Keyboard.pressNative( 0x58 );   break;  // KEYPAD ENTER
            case  0x0031:   Keyboard.pressNative( 0x59 );   break;  // KEYPAD 1
            case  0x0032:   Keyboard.pressNative( 0x5a );   break;  // KEYPAD 2
            case  0x0033:   Keyboard.pressNative( 0x5b );   break;  // KEYPAD 3
            case  0x0034:   Keyboard.pressNative( 0x5c );   break;  // KEYPAD 4
            case  0x0035:   Keyboard.pressNative( 0x5d );   break;  // KEYPAD 5
            case  0x0036:   Keyboard.pressNative( 0x5e );   break;  // KEYPAD 6
            case  0x0037:   Keyboard.pressNative( 0x5f );   break;  // KEYPAD 7
            case  0x0038:   Keyboard.pressNative( 0x60 );   break;  // KEYPAD 8
            case  0x0039:   Keyboard.pressNative( 0x61 );   break;  // KEYPAD 9
            case  0x0030:   Keyboard.pressNative( 0x62 );   break;  // KEYPAD 0
            case  0x0071:   Keyboard.pressNative( 0x3a );   break;  // F1
            case  0x0072:   Keyboard.pressNative( 0x3b );   break;  // F2
            case  0x0073:   Keyboard.pressNative( 0x3c );   break;  // F3
            case  0x0074:   Keyboard.pressNative( 0x3d );   break;  // F4
            case  0x0075:   Keyboard.pressNative( 0x3e );   break;  // F5
            case  0x00e1:   Keyboard.pressNative( 0x42 );   break;  // F9 <- [COPY]
            case  0x00e2:   Keyboard.pressNative( 0x41 );   break;  // F8 <- [HELP]
            case  0x00e8:   Keyboard.pressNative( 0x62, 0x32 );   break;  // FULL ] <- XFER
            case  0x00eb:   Keyboard.pressNative( 0x40 );   break;  // F7 <- [RollDown]
            case  0x00ec:   Keyboard.pressNative( 0x3f );   break;  // F6 <- [RollUp]
            default:  break;
            }
          } else {
            // フルキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x0020:  Keyboard.pressNative( 0x2c );   break;  // SPACE
            case  0x0008:  Keyboard.pressNative( 0x4c );   break;  // DEL
            case  0x0009:  Keyboard.pressNative( 0x2b );   break;  // TAB
            case  0x000d:  Keyboard.pressNative( 0x28 );   break;  // Enter
            case  0x0013:  Keyboard.pressNative( 0x48 );   break;  // Pause <- BREAK
            case  0x001b:  Keyboard.pressNative( 0x29 );   break;  // ESC
            case  0x00ce:  Keyboard.pressNative( 0x2d );   break;  // ホ
            case  0x00cd:  Keyboard.pressNative( 0x2e );   break;  // ヘ
            case  0x00b0:  Keyboard.pressNative( 0x89 );   break;  // ー
            case  0x00de:  Keyboard.pressNative( 0x2f );   break;  // ゛
            case  0x00df:  Keyboard.pressNative( 0x30 );   break;  // ゜
            case  0x00da:  Keyboard.pressNative( 0x33 );   break;  // レ
            case  0x00b9:  Keyboard.pressNative( 0x34 );   break;  // ケ
            case  0x00d1:  Keyboard.pressNative( 0x32 );   break;  // ム
            case  0x00c8:  Keyboard.pressNative( 0x36 );   break;  // ネ
            case  0x00d9:  Keyboard.pressNative( 0x37 );   break;  // ル
            case  0x00d2:  Keyboard.pressNative( 0x38 );   break;  // メ
            case  0x00db:  Keyboard.pressNative( 0x87 );   break;  // ロ
            case  0x00c7:  Keyboard.pressNative( 0x1e );   break;  // ヌ
            case  0x00cc:  Keyboard.pressNative( 0x1f );   break;  // フ
            case  0x00b1:  Keyboard.pressNative( 0x20 );   break;  // ア
            case  0x00b3:  Keyboard.pressNative( 0x21 );   break;  // ウ
            case  0x00b4:  Keyboard.pressNative( 0x22 );   break;  // エ
            case  0x00b5:  Keyboard.pressNative( 0x23 );   break;  // オ
            case  0x00d4:  Keyboard.pressNative( 0x24 );   break;  // ヤ
            case  0x00d5:  Keyboard.pressNative( 0x25 );   break;  // ユ
            case  0x00d6:  Keyboard.pressNative( 0x26 );   break;  // ヨ
            case  0x00dc:  Keyboard.pressNative( 0x27 );   break;  // ワ
            case  0x00c1:  Keyboard.pressNative( 0x04 );   break;  // チ
            case  0x00ba:  Keyboard.pressNative( 0x05 );   break;  // コ
            case  0x00bf:  Keyboard.pressNative( 0x06 );   break;  // ソ
            case  0x00bc:  Keyboard.pressNative( 0x07 );   break;  // シ
            case  0x00b2:  Keyboard.pressNative( 0x08 );   break;  // エ
            case  0x00ca:  Keyboard.pressNative( 0x09 );   break;  // ハ
            case  0x00b7:  Keyboard.pressNative( 0x0a );   break;  // キ
            case  0x00b8:  Keyboard.pressNative( 0x0b );   break;  // ク
            case  0x00c6:  Keyboard.pressNative( 0x0c );   break;  // ニ
            case  0x00cf:  Keyboard.pressNative( 0x0d );   break;  // マ
            case  0x00c9:  Keyboard.pressNative( 0x0e );   break;  // ノ
            case  0x00d8:  Keyboard.pressNative( 0x0f );   break;  // リ
            case  0x00d3:  Keyboard.pressNative( 0x10 );   break;  // モ
            case  0x00d0:  Keyboard.pressNative( 0x11 );   break;  // ミ
            case  0x00d7:  Keyboard.pressNative( 0x12 );   break;  // ラ
            case  0x00be:  Keyboard.pressNative( 0x13 );   break;  // セ
            case  0x00c0:  Keyboard.pressNative( 0x14 );   break;  // タ
            case  0x00bd:  Keyboard.pressNative( 0x15 );   break;  // ス
            case  0x00c4:  Keyboard.pressNative( 0x16 );   break;  // ト
            case  0x00b6:  Keyboard.pressNative( 0x17 );   break;  // カ
            case  0x00c5:  Keyboard.pressNative( 0x18 );   break;  // ナ
            case  0x00cb:  Keyboard.pressNative( 0x19 );   break;  // ヒ
            case  0x00c3:  Keyboard.pressNative( 0x1a );   break;  // テ
            case  0x00bb:  Keyboard.pressNative( 0x1b );   break;  // サ
            case  0x00dd:  Keyboard.pressNative( 0x1c );   break;  // ン
            case  0x00c2:  Keyboard.pressNative( 0x1d );   break;  // ツ
            default:  break;
            }
          }
        }
      //--------------------------------------------------------------------------------
      } else {
        // かなキーなし
        if( (code & D_KEY_MASK_SHIFT) == 0 ) {
          // シフトキー押下状態
          if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
            // テンキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x000c:   Keyboard.pressNative( 0xe1, 0x4a );   break;  // CLR(HOME)
            case  0x001d:   Keyboard.pressNative( 0xe1, 0x50 );   break;  // KEYPAD ←
            case  0x001c:   Keyboard.pressNative( 0xe1, 0x4f );   break;  // KEYPAD →
            case  0x001e:   Keyboard.pressNative( 0xe1, 0x52 );   break;  // KEYPAD ↑
            case  0x001f:   Keyboard.pressNative( 0xe1, 0x51 );   break;  // KEYPAD ↓
            case  0x002a:   Keyboard.pressNative( 0xe1, 0x55 );   break;  // KEYPAD *
            case  0x002b:   Keyboard.pressNative( 0xe1, 0x57 );   break;  // KEYPAD +
            case  0x002c:   Keyboard.pressNative( 0xe1, 0x36 );   break;  // FULL , <- KEYPAD ,
            case  0x002e:   Keyboard.pressNative( 0xe1, 0x63 );   break;  // KEYPAD .
            case  0x002d:   Keyboard.pressNative( 0xe1, 0x56 );   break;  // KEYPAD -
            case  0x002f:   Keyboard.pressNative( 0xe1, 0x54 );   break;  // KEYPAD /
            case  0x003d:   Keyboard.pressNative( 0xe1, 0x2d );   break;  // FULL SHIFT - <- KEYPAD
            case  0x000d:   Keyboard.pressNative( 0xe1, 0x58 );   break;  // KEYPAD ENTER
            case  0x0031:   Keyboard.pressNative( 0xe1, 0x59 );   break;  // KEYPAD 1
            case  0x0032:   Keyboard.pressNative( 0xe1, 0x5a );   break;  // KEYPAD 2
            case  0x0033:   Keyboard.pressNative( 0xe1, 0x5b );   break;  // KEYPAD 3
            case  0x0034:   Keyboard.pressNative( 0xe1, 0x5c );   break;  // KEYPAD 4
            case  0x0035:   Keyboard.pressNative( 0xe1, 0x5d );   break;  // KEYPAD 5
            case  0x0036:   Keyboard.pressNative( 0xe1, 0x5e );   break;  // KEYPAD 6
            case  0x0037:   Keyboard.pressNative( 0xe1, 0x5f );   break;  // KEYPAD 7
            case  0x0038:   Keyboard.pressNative( 0xe1, 0x60 );   break;  // KEYPAD 8
            case  0x0039:   Keyboard.pressNative( 0xe1, 0x61 );   break;  // KEYPAD 9
            case  0x0030:   Keyboard.pressNative( 0xe1, 0x62 );   break;  // KEYPAD 0
            case  0x0076:   Keyboard.pressNative( 0xe1, 0x3a );   break;  // F1
            case  0x0077:   Keyboard.pressNative( 0xe1, 0x3b );   break;  // F2
            case  0x0078:   Keyboard.pressNative( 0xe1, 0x3c );   break;  // F3
            case  0x0079:   Keyboard.pressNative( 0xe1, 0x3d );   break;  // F4
            case  0x007a:   Keyboard.pressNative( 0xe1, 0x3e );   break;  // F5
            case  0x00e1:   Keyboard.pressNative( 0x42 );   break;  // F9 <- [COPY]
            case  0x00e2:   Keyboard.pressNative( 0x41 );   break;  // F8 <- [HELP]
            case  0x00e8:   Keyboard.pressNative( 0x62, 0x32 );   break;  // FULL ] <- XFER
            case  0x00eb:   Keyboard.pressNative( 0x40 );   break;  // F7 <- [RollDown]
            case  0x00ec:   Keyboard.pressNative( 0x3f );   break;  // F6 <- [RollUp]
            default:  Keyboard.pressNative( 0xe1 ); break;
            }
          } else {
            // フルキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x0020:   Keyboard.pressNative( 0xe1, 0x2c );   break;  // SPACE
            case  0x0012:   Keyboard.pressNative( 0xe1, 0x49 );   break;  // INS(DEL)
            case  0x0009:   Keyboard.pressNative( 0xe1, 0x2b );   break;  // TAB
            case  0x000d:   Keyboard.pressNative( 0xe1, 0x28 );   break;  // Enter
            case  0x0003:   Keyboard.pressNative( 0xe1, 0x48 );   break;  // Pause <- BREAK
            case  0x001b:   Keyboard.pressNative( 0xe1, 0x29 );   break;  // ESC
            case  0x003d:   Keyboard.pressNative( 0xe1, 0x2d );   break;  // =
            case  0x007e:   Keyboard.pressNative( 0xe1, 0x2e );   break;  // ~
            case  0x007c:   Keyboard.pressNative( 0xe1, 0x89 );   break;  // |
            case  0x0060:   Keyboard.pressNative( 0xe1, 0x2f );   break;  // '
            case  0x007b:   Keyboard.pressNative( 0xe1, 0x30 );   break;  // {
            case  0x002b:   Keyboard.pressNative( 0xe1, 0x33 );   break;  // +
            case  0x002a:   Keyboard.pressNative( 0xe1, 0x34 );   break;  // *
            case  0x007d:   Keyboard.pressNative( 0xe1, 0x32 );   break;  // }
            case  0x003c:   Keyboard.pressNative( 0xe1, 0x36 );   break;  // <
            case  0x003e:   Keyboard.pressNative( 0xe1, 0x37 );   break;  // >
            case  0x003f:   Keyboard.pressNative( 0xe1, 0x38 );   break;  // ?
            case  0x005f:   Keyboard.pressNative( 0xe1, 0x87 );   break;  // (\)
            case  0x0021:   Keyboard.pressNative( 0xe1, 0x1e );   break;  // !
            case  0x0022:   Keyboard.pressNative( 0xe1, 0x1f );   break;  // "
            case  0x0023:   Keyboard.pressNative( 0xe1, 0x20 );   break;  // #
            case  0x0024:   Keyboard.pressNative( 0xe1, 0x21 );   break;  // $
            case  0x0025:   Keyboard.pressNative( 0xe1, 0x22 );   break;  // %
            case  0x0026:   Keyboard.pressNative( 0xe1, 0x23 );   break;  // &
            case  0x0027:   Keyboard.pressNative( 0xe1, 0x24 );   break;  // '
            case  0x0028:   Keyboard.pressNative( 0xe1, 0x25 );   break;  // (
            case  0x0029:   Keyboard.pressNative( 0xe1, 0x26 );   break;  // )
            case  0x0041:   Keyboard.pressNative( 0xe1, 0x04 );   break;  // A
            case  0x0042:   Keyboard.pressNative( 0xe1, 0x05 );   break;  // B
            case  0x0043:   Keyboard.pressNative( 0xe1, 0x06 );   break;  // C
            case  0x0044:   Keyboard.pressNative( 0xe1, 0x07 );   break;  // D
            case  0x0045:   Keyboard.pressNative( 0xe1, 0x08 );   break;  // E
            case  0x0046:   Keyboard.pressNative( 0xe1, 0x09 );   break;  // F
            case  0x0047:   Keyboard.pressNative( 0xe1, 0x0a );   break;  // G
            case  0x0048:   Keyboard.pressNative( 0xe1, 0x0b );   break;  // H
            case  0x0049:   Keyboard.pressNative( 0xe1, 0x0c );   break;  // I
            case  0x004a:   Keyboard.pressNative( 0xe1, 0x0d );   break;  // J
            case  0x004b:   Keyboard.pressNative( 0xe1, 0x0e );   break;  // K
            case  0x004c:   Keyboard.pressNative( 0xe1, 0x0f );   break;  // L
            case  0x004d:   Keyboard.pressNative( 0xe1, 0x10 );   break;  // M
            case  0x004e:   Keyboard.pressNative( 0xe1, 0x11 );   break;  // N
            case  0x004f:   Keyboard.pressNative( 0xe1, 0x12 );   break;  // O
            case  0x0050:   Keyboard.pressNative( 0xe1, 0x13 );   break;  // P
            case  0x0051:   Keyboard.pressNative( 0xe1, 0x14 );   break;  // Q
            case  0x0052:   Keyboard.pressNative( 0xe1, 0x15 );   break;  // R
            case  0x0053:   Keyboard.pressNative( 0xe1, 0x16 );   break;  // S
            case  0x0054:   Keyboard.pressNative( 0xe1, 0x17 );   break;  // T
            case  0x0055:   Keyboard.pressNative( 0xe1, 0x18 );   break;  // U
            case  0x0056:   Keyboard.pressNative( 0xe1, 0x19 );   break;  // V
            case  0x0057:   Keyboard.pressNative( 0xe1, 0x1a );   break;  // W
            case  0x0058:   Keyboard.pressNative( 0xe1, 0x1b );   break;  // X
            case  0x0059:   Keyboard.pressNative( 0xe1, 0x1c );   break;  // Y
            case  0x005a:   Keyboard.pressNative( 0xe1, 0x1d );   break;  // Z
            case  0x0061:   Keyboard.pressNative( 0x04 );   break;  // A
            case  0x0062:   Keyboard.pressNative( 0x05 );   break;  // B
            case  0x0063:   Keyboard.pressNative( 0x06 );   break;  // C
            case  0x0064:   Keyboard.pressNative( 0x07 );   break;  // D
            case  0x0065:   Keyboard.pressNative( 0x08 );   break;  // E
            case  0x0066:   Keyboard.pressNative( 0x09 );   break;  // F
            case  0x0067:   Keyboard.pressNative( 0x0a );   break;  // G
            case  0x0068:   Keyboard.pressNative( 0x0b );   break;  // H
            case  0x0069:   Keyboard.pressNative( 0x0c );   break;  // I
            case  0x006a:   Keyboard.pressNative( 0x0d );   break;  // J
            case  0x006b:   Keyboard.pressNative( 0x0e );   break;  // K
            case  0x006c:   Keyboard.pressNative( 0x0f );   break;  // L
            case  0x006d:   Keyboard.pressNative( 0x10 );   break;  // M
            case  0x006e:   Keyboard.pressNative( 0x11 );   break;  // N
            case  0x006f:   Keyboard.pressNative( 0x12 );   break;  // O
            case  0x0070:   Keyboard.pressNative( 0x13 );   break;  // P
            case  0x0071:   Keyboard.pressNative( 0x14 );   break;  // Q
            case  0x0072:   Keyboard.pressNative( 0x15 );   break;  // R
            case  0x0073:   Keyboard.pressNative( 0x16 );   break;  // S
            case  0x0074:   Keyboard.pressNative( 0x17 );   break;  // T
            case  0x0075:   Keyboard.pressNative( 0x18 );   break;  // U
            case  0x0076:   Keyboard.pressNative( 0x19 );   break;  // V
            case  0x0077:   Keyboard.pressNative( 0x1a );   break;  // W
            case  0x0078:   Keyboard.pressNative( 0x1b );   break;  // X
            case  0x0079:   Keyboard.pressNative( 0x1c );   break;  // Y
            case  0x007a:   Keyboard.pressNative( 0x1d );   break;  // Z
            default:  Keyboard.pressNative( 0xe1 ); break;
            }
          }
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        } else {
          // シフトキーなし
          if( (code & D_KEY_MASK_TENKEY) == 0x0000 ) {
            // テンキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x000b:   Keyboard.pressNative( 0x4a );   break;  // HOME
            case  0x001d:   Keyboard.pressNative( 0x50 );   break;  // KEYPAD ←
            case  0x001c:   Keyboard.pressNative( 0x4f );   break;  // KEYPAD →
            case  0x001e:   Keyboard.pressNative( 0x52 );   break;  // KEYPAD ↑
            case  0x001f:   Keyboard.pressNative( 0x51 );   break;  // KEYPAD ↓
            case  0x002a:   Keyboard.pressNative( 0x55 );   break;  // KEYPAD *
            case  0x002b:   Keyboard.pressNative( 0x57 );   break;  // KEYPAD +
            case  0x002c:   Keyboard.pressNative( 0x36 );   break;  // FULL , <- KEYPAD ,
            case  0x002e:   Keyboard.pressNative( 0x63 );   break;  // KEYPAD .
            case  0x002d:   Keyboard.pressNative( 0x56 );   break;  // KEYPAD -
            case  0x002f:   Keyboard.pressNative( 0x54 );   break;  // KEYPAD /
            case  0x003d:   Keyboard.pressNative( 0xe1, 0x2d );  break;  // FULL SHIFT - <- KEYPAD
            case  0x000d:   Keyboard.pressNative( 0x58 );   break;  // KEYPAD ENTER
            case  0x0031:   Keyboard.pressNative( 0x59 );   break;  // KEYPAD 1
            case  0x0032:   Keyboard.pressNative( 0x5a );   break;  // KEYPAD 2
            case  0x0033:   Keyboard.pressNative( 0x5b );   break;  // KEYPAD 3
            case  0x0034:   Keyboard.pressNative( 0x5c );   break;  // KEYPAD 4
            case  0x0035:   Keyboard.pressNative( 0x5d );   break;  // KEYPAD 5
            case  0x0036:   Keyboard.pressNative( 0x5e );   break;  // KEYPAD 6
            case  0x0037:   Keyboard.pressNative( 0x5f );   break;  // KEYPAD 7
            case  0x0038:   Keyboard.pressNative( 0x60 );   break;  // KEYPAD 8
            case  0x0039:   Keyboard.pressNative( 0x61 );   break;  // KEYPAD 9
            case  0x0030:   Keyboard.pressNative( 0x62 );   break;  // KEYPAD 0
            case  0x0071:   Keyboard.pressNative( 0x3a );   break;  // F1
            case  0x0072:   Keyboard.pressNative( 0x3b );   break;  // F2
            case  0x0073:   Keyboard.pressNative( 0x3c );   break;  // F3
            case  0x0074:   Keyboard.pressNative( 0x3d );   break;  // F4
            case  0x0075:   Keyboard.pressNative( 0x3e );   break;  // F5
            case  0x00e1:   Keyboard.pressNative( 0x42 );   break;  // F9 <- [COPY]
            case  0x00e2:   Keyboard.pressNative( 0x41 );   break;  // F8 <- [HELP]
            case  0x00e8:   Keyboard.pressNative( 0x62, 0x32 );   break;  // FULL ] <- XFER
            case  0x00eb:   Keyboard.pressNative( 0x40 );   break;  // F7 <- [RollDown]
            case  0x00ec:   Keyboard.pressNative( 0x3f );   break;  // F6 <- [RollUp]
            default:  break;
            }
          } else {
            // フルキー側
            switch( code & D_KEY_MASK_KEYCODE ) {
            case  0x0020:  Keyboard.pressNative( 0x2c );   break;  // SPACE
            case  0x0008:  Keyboard.pressNative( 0x4c );   break;  // DEL
            case  0x0009:  Keyboard.pressNative( 0x2b );   break;  // TAB
            case  0x000d:  Keyboard.pressNative( 0x28 );   break;  // Enter
            case  0x0013:  Keyboard.pressNative( 0x48 );   break;  // Pause <- BREAK
            case  0x001b:  Keyboard.pressNative( 0x29 );   break;  // ESC
            case  0x002d:  Keyboard.pressNative( 0x2d );   break;  // -
            case  0x005e:  Keyboard.pressNative( 0x2e );   break;  // ^
            case  0x005c:  Keyboard.pressNative( 0x89 );   break;  // \
            case  0x0040:  Keyboard.pressNative( 0x2f );   break;  // @
            case  0x005b:  Keyboard.pressNative( 0x30 );   break;  // [
            case  0x003b:  Keyboard.pressNative( 0x33 );   break;  // ;
            case  0x003a:  Keyboard.pressNative( 0x34 );   break;  // :
            case  0x005d:  Keyboard.pressNative( 0x32 );   break;  // ]
            case  0x002c:  Keyboard.pressNative( 0x36 );   break;  // ,
            case  0x002e:  Keyboard.pressNative( 0x37 );   break;  // .
            case  0x002f:  Keyboard.pressNative( 0x38 );   break;  // /
            case  0x005f:  Keyboard.pressNative( 0x87 );   break;  // (\)
            case  0x0031:  Keyboard.pressNative( 0x1e );   break;  // 1
            case  0x0032:  Keyboard.pressNative( 0x1f );   break;  // 2
            case  0x0033:  Keyboard.pressNative( 0x20 );   break;  // 3
            case  0x0034:  Keyboard.pressNative( 0x21 );   break;  // 4
            case  0x0035:  Keyboard.pressNative( 0x22 );   break;  // 5
            case  0x0036:  Keyboard.pressNative( 0x23 );   break;  // 6
            case  0x0037:  Keyboard.pressNative( 0x24 );   break;  // 7
            case  0x0038:  Keyboard.pressNative( 0x25 );   break;  // 8
            case  0x0039:  Keyboard.pressNative( 0x26 );   break;  // 9
            case  0x0030:  Keyboard.pressNative( 0x27 );   break;  // 0
            case  0x0061:  Keyboard.pressNative( 0x04 );   break;  // a
            case  0x0062:  Keyboard.pressNative( 0x05 );   break;  // b
            case  0x0063:  Keyboard.pressNative( 0x06 );   break;  // c
            case  0x0064:  Keyboard.pressNative( 0x07 );   break;  // d
            case  0x0065:  Keyboard.pressNative( 0x08 );   break;  // e
            case  0x0066:  Keyboard.pressNative( 0x09 );   break;  // f
            case  0x0067:  Keyboard.pressNative( 0x0a );   break;  // g
            case  0x0068:  Keyboard.pressNative( 0x0b );   break;  // h
            case  0x0069:  Keyboard.pressNative( 0x0c );   break;  // i
            case  0x006a:  Keyboard.pressNative( 0x0d );   break;  // j
            case  0x006b:  Keyboard.pressNative( 0x0e );   break;  // k
            case  0x006c:  Keyboard.pressNative( 0x0f );   break;  // l
            case  0x006d:  Keyboard.pressNative( 0x10 );   break;  // m
            case  0x006e:  Keyboard.pressNative( 0x11 );   break;  // n
            case  0x006f:  Keyboard.pressNative( 0x12 );   break;  // o
            case  0x0070:  Keyboard.pressNative( 0x13 );   break;  // p
            case  0x0071:  Keyboard.pressNative( 0x14 );   break;  // q
            case  0x0072:  Keyboard.pressNative( 0x15 );   break;  // r
            case  0x0073:  Keyboard.pressNative( 0x16 );   break;  // s
            case  0x0074:  Keyboard.pressNative( 0x17 );   break;  // t
            case  0x0075:  Keyboard.pressNative( 0x18 );   break;  // u
            case  0x0076:  Keyboard.pressNative( 0x19 );   break;  // v
            case  0x0077:  Keyboard.pressNative( 0x1a );   break;  // w
            case  0x0078:  Keyboard.pressNative( 0x1b );   break;  // x
            case  0x0079:  Keyboard.pressNative( 0x1c );   break;  // y
            case  0x007a:  Keyboard.pressNative( 0x1d );   break;  // z
            case  0x0041:  Keyboard.pressNative( 0xe1, 0x04 );   break;  // a
            case  0x0042:  Keyboard.pressNative( 0xe1, 0x05 );   break;  // b
            case  0x0043:  Keyboard.pressNative( 0xe1, 0x06 );   break;  // c
            case  0x0044:  Keyboard.pressNative( 0xe1, 0x07 );   break;  // d
            case  0x0045:  Keyboard.pressNative( 0xe1, 0x08 );   break;  // e
            case  0x0046:  Keyboard.pressNative( 0xe1, 0x09 );   break;  // f
            case  0x0047:  Keyboard.pressNative( 0xe1, 0x0a );   break;  // g
            case  0x0048:  Keyboard.pressNative( 0xe1, 0x0b );   break;  // h
            case  0x0049:  Keyboard.pressNative( 0xe1, 0x0c );   break;  // i
            case  0x004a:  Keyboard.pressNative( 0xe1, 0x0d );   break;  // j
            case  0x004b:  Keyboard.pressNative( 0xe1, 0x0e );   break;  // k
            case  0x004c:  Keyboard.pressNative( 0xe1, 0x0f );   break;  // l
            case  0x004d:  Keyboard.pressNative( 0xe1, 0x10 );   break;  // m
            case  0x004e:  Keyboard.pressNative( 0xe1, 0x11 );   break;  // n
            case  0x004f:  Keyboard.pressNative( 0xe1, 0x12 );   break;  // o
            case  0x0050:  Keyboard.pressNative( 0xe1, 0x13 );   break;  // p
            case  0x0051:  Keyboard.pressNative( 0xe1, 0x14 );   break;  // q
            case  0x0052:  Keyboard.pressNative( 0xe1, 0x15 );   break;  // r
            case  0x0053:  Keyboard.pressNative( 0xe1, 0x16 );   break;  // s
            case  0x0054:  Keyboard.pressNative( 0xe1, 0x17 );   break;  // t
            case  0x0055:  Keyboard.pressNative( 0xe1, 0x18 );   break;  // u
            case  0x0056:  Keyboard.pressNative( 0xe1, 0x19 );   break;  // v
            case  0x0057:  Keyboard.pressNative( 0xe1, 0x1a );   break;  // w
            case  0x0058:  Keyboard.pressNative( 0xe1, 0x1b );   break;  // x
            case  0x0059:  Keyboard.pressNative( 0xe1, 0x1c );   break;  // y
            case  0x005a:  Keyboard.pressNative( 0xe1, 0x1d );   break;  // z
            default:  break;
            }
          }
        }
      }
      //--------------------------------------------------------------------------------
    } else {
      // キーなし
      Keyboard.releaseAll();
    }
  } else {
    // 入力がない場合、待ち
    delay( 30 );
  }

}
