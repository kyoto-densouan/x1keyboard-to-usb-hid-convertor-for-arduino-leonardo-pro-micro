#ifndef D_X1_KEYBOARD_H
#define D_X1_KEYBOARD_H

#include "FifoBuffer.h"

class X1Keyboard
{
public:
  static int pin_;
  static int interrupt_;

  static unsigned long bits_;
  static int bits_count_;
  static int bits_length_;
  static unsigned long last_tick_;
  static unsigned long lo_tick_;
  static unsigned long hi_tick_;
  static const int D_BIT_LENGTH_TOLERANCE  = (50);
  
  static const int D_STATE_HEADER          = (0);
  static const int D_STATE_COMMON_DATA     = (1);
  static const int D_STATE_TURBO_DATA      = (2);
  static const int D_STATE_FOOTER          = (3);
  static int state_;

  static FifoBuffer  inkey_buff_;

public:
  X1Keyboard( int pin, int interrupt ) {
    pin_ = pin;
    interrupt_ = interrupt;

    bits_ = 0;
    bits_count_ = 0;
    bits_length_ = 18;
    last_tick_ = 0;
    lo_tick_ = 0;
    hi_tick_ = 0;

    state_ = D_STATE_HEADER;

    inkey_buff_.clear();

    pinMode( pin_, INPUT );
    attachInterrupt( interrupt_, interrupt_func, CHANGE );
  }

  static bool check( unsigned long interval, unsigned long target, unsigned long tolerance ) {
    bool result = false;
    if( ((target - tolerance) < interval) && ((target + tolerance) > interval) ) {
      result = true;
    }
    return result;
  }
  
  static void interrupt_func() {
    unsigned long now = micros();
    unsigned long dt = now - last_tick_;
    last_tick_ = now;
  
    int r = digitalRead( pin_ );
  
    switch( state_ ) {
      case D_STATE_HEADER:
        {
          if( r == HIGH ){
            lo_tick_ = dt;
          }else{
            hi_tick_ = dt;
        
            if( check( lo_tick_, 1000, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 700, D_BIT_LENGTH_TOLERANCE ) ) {
              state_ = D_STATE_COMMON_DATA;
              bits_ = 0;
              bits_count_ = 0;
              bits_length_ = 16;
            } else if( check( lo_tick_, 400, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 200, D_BIT_LENGTH_TOLERANCE ) ) {
              state_ = D_STATE_TURBO_DATA;
              bits_ = 0;
              bits_count_ = 0;
              bits_length_ = 24;
            } else {
              bits_ = 0;
              bits_count_ = 0;
            }
          }
        }
        break;
      case D_STATE_COMMON_DATA:
        {
          if( r == HIGH ){
            lo_tick_ = dt;
          }else{
            hi_tick_ = dt;
        
            if( check( lo_tick_, 250, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 750, D_BIT_LENGTH_TOLERANCE ) ) {
              bits_ <<= 1;
              bits_ |= 0;
              bits_count_++;
            } else if( check( lo_tick_, 250, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 1750, D_BIT_LENGTH_TOLERANCE ) ) {
              bits_ <<= 1;
              bits_ |= 1;
              bits_count_++;
            } else {
              state_ = D_STATE_HEADER;
              bits_ = 0;
              bits_count_ = 0;
            }
          }
          if( bits_count_ >= bits_length_ ) {
            state_ = D_STATE_FOOTER;
          }
        }
        break;
      case D_STATE_TURBO_DATA:
        {
          if( r == HIGH ){
            lo_tick_ = dt;
          }else{
            hi_tick_ = dt;
  
            if( check( lo_tick_, 250, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 250, D_BIT_LENGTH_TOLERANCE ) ) {
              bits_ <<= 1;
              bits_ |= 0;
              bits_count_++;
            } else if( check( lo_tick_, 250, D_BIT_LENGTH_TOLERANCE ) && check( hi_tick_, 750, D_BIT_LENGTH_TOLERANCE ) ) {
              bits_ <<= 1;
              bits_ |= 1;
              bits_count_++;
            } else {
              state_ = D_STATE_HEADER;
              bits_ = 0;
              bits_count_ = 0;
            }
          }
          if( bits_count_ >= bits_length_ ) {
            state_ = D_STATE_FOOTER;
          }
        }
        break;
      case D_STATE_FOOTER:
        {
          if( r == HIGH ){
            if( bits_count_ == 16 ) {
              inkey_buff_.push( bits_ );
            }
  
            state_ = D_STATE_HEADER;
            bits_ = 0;
            bits_count_ = 0;
          }        
        }
        break;
      default:
        break;
    }
  }

  int size() {
    return  inkey_buff_.size();
  }

  unsigned long get_code() {
    return inkey_buff_.pull();
  }
  
};

int           X1Keyboard::pin_          = 2;
int           X1Keyboard::interrupt_    = 1;
FifoBuffer    X1Keyboard::inkey_buff_;
unsigned long X1Keyboard::bits_ = 0;
int           X1Keyboard::bits_count_   = 0;
int           X1Keyboard::bits_length_  = 0;
unsigned long X1Keyboard::last_tick_    = 0;
unsigned long X1Keyboard::lo_tick_      = 0;
unsigned long X1Keyboard::hi_tick_      = 0;
int           X1Keyboard::state_        = 0;

#endif
