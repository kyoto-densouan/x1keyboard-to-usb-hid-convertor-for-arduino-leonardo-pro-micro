#ifndef D_FIFO_BUFFER_H
#define D_FIFO_BUFFER_H

class FifoBuffer
{
private:
  static const int D_INKEY_BUFF_LENGTH = (16);
  unsigned long buff_[D_INKEY_BUFF_LENGTH];
  int in_ = 0;
  int out_ = 0;
  int count_ = 0;
  
public:
  FifoBuffer() {
    clear();
  }

  void clear() {
    for( int i = 0; i < D_INKEY_BUFF_LENGTH; i++ ) {
      buff_[i] = 0;
    }
    in_ = 0;
    out_ = 0;
    count_ = 0;
  }

  void push( unsigned long d ) {
    buff_[in_] = d;
    in_++;
    count_++;
  
    if( in_ >= D_INKEY_BUFF_LENGTH ) {
      in_ = 0;
    }
    if( count_ >= D_INKEY_BUFF_LENGTH ) {
      out_++;
      if( out_ >= D_INKEY_BUFF_LENGTH ) {
        out_ = 0;
      }
      count_--;
    }
  }
  
  unsigned long pull() {
    unsigned long result = 0;
    if( count_ > 0 ) {
      result = buff_[out_];
      out_++;
      if( out_ >= D_INKEY_BUFF_LENGTH ) {
        out_ = 0;
      }
      count_--;
    }
    return result;
  }
  
  int size() {
    return count_;
  }

};

#endif

